import './jquery-1.4.2';


export function initGraph() {
    console.log('init graph');
    for (var i = 0; i < 10; i++) {
        console.log(i);
        let trs = '';
        trs += '<tr>';
        for (var j = 0; j < 10; j++) {
            let tds = '';

            tds += '<td id="p' + j + i + '" class="elem"></td>';


            $(`#${j}${i}`).attr('height', '50px');
            $(`#${j}${i}`).attr('width', '50px');
            trs += tds;

        }


        trs += '</tr>';

        $('#tab').append(trs);
    }

    bindClicks();
}

function handleClick() {

}

function bindClicks() {
    document.addEventListener('click', function (e) {
        if (hasClass(e.target, 'elem')) {
            console.log(e.target.id);
        }
    }, false);

}


export function changeCellStyle(cell, cssClass) {
    $(`#p${cell}`).addClass(cssClass);
}

export function setPlayerInPosition(cell){
    $(`#p${cell}`).append(`
    <img src="player.png" height="20px" width="20px"/>
    `)
}

function hasClass(elem, className) {
    return elem.className.split(' ').indexOf(className) > -1;
}